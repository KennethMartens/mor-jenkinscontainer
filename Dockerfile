FROM jenkins/jenkins:lts
LABEL maintainer="kenneth.martens@asml.com"

#Install build dependencoes
USER root
RUN curl -sL https://deb.nodesource.com/setup_11.x | bash - && apt-get install -y nodejs

USER jenkins
#Disable the security wizard
ENV JAVA_OPTS "-Djenkins.install.runSetupWizard=false ${JAVA_OPTS:-}"

#Copy files to right locations.
COPY groovy/executors.groovy /usr/share/jenkins/ref/init.groovy.d/executors.groovy
COPY groovy/security.groovy /usr/share/jenkins/ref/init.groovy.d/security.groovy
COPY groovy/sshcredentials.groovy /usr/share/jenkins/ref/init.groovy.d/sshcredentials.groovy
COPY groovy/addNodeToJenkins.groovy /usr/share/jenkins/ref/init.groovy.d/zAddNodeToJenkins.groovy

#Plugins
COPY configuration/plugins.txt /usr/share/jenkins/ref/plugins.txt
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt