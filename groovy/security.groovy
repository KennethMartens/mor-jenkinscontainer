#!groovy
import java.util.logging.Level
import java.util.logging.Logger
import hudson.security.*
import jenkins.model.*
import hudson.security.csrf.DefaultCrumbIssuer
import java.io.File

def admin_username = "jenkinsadmin"
def admin_password = new File("/run/secrets/JenkinsSecret").text;

def instance = Jenkins.getInstance()
def logger = Logger.getLogger(Jenkins.class.getName())

logger.log(Level.INFO, "Ensuring that local user " + admin_username +" is created.")
if (!instance.isUseSecurity()) {
    logger.log(Level.INFO, "Creating local admin user "+ admin_username +".")

    def hudsonRealm = new HudsonPrivateSecurityRealm(false)
    hudsonRealm.createAccount(admin_username, admin_password)

    def strategy = new FullControlOnceLoggedInAuthorizationStrategy()
    strategy.setAllowAnonymousRead(false)

    instance.setSecurityRealm(hudsonRealm)
    instance.setAuthorizationStrategy(strategy)

    logger.log(Level.INFO, "Setting remote CLI to false");
    //Disable Jenkins CLI via remoting
    instance.getDescriptor("jenkins.CLI").get().setEnabled(false)
    //Enforce Cross Server Request Forgery protection.
    logger.log(Level.INFO, "Setting default crumb issuer.");
    instance.setCrumbIssuer(new DefaultCrumbIssuer(true))

    instance.save()
}