#!groovy

// imports
import java.util.logging.Level
import java.util.logging.Logger
import com.cloudbees.jenkins.plugins.sshcredentials.impl.*
import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.common.*
import com.cloudbees.plugins.credentials.domains.Domain
import com.cloudbees.plugins.credentials.impl.*
import hudson.util.Secret
import java.nio.file.Files
import jenkins.model.Jenkins
import net.sf.json.JSONObject
import org.jenkinsci.plugins.plaincredentials.impl.*

// get Jenkins instance
Jenkins jenkins = Jenkins.getInstance()
def logger = Logger.getLogger(Jenkins.class.getName())

// parameters
def jenkinsOAuthKey =  "kyrU2vGSTL4ELfPERV";
def jenkinsOAuthSecret = new File("/run/secrets/bitbucketOauthSecret").text;

String id = java.util.UUID.randomUUID().toString()
Credentials c = new UsernamePasswordCredentialsImpl(CredentialsScope.GLOBAL, id, "Bitbucket OAUTH key for Jenkins Bitbucket Notifier", jenkinsOAuthKey, jenkinsOAuthSecret)

SystemCredentialsProvider.getInstance().getStore().addCredentials(Domain.global(), c)

// save to disk
jenkins.save()

import jenkins.model.*
import jenkins.plugins.publish_over_ssh.BapSshHostConfiguration
def inst = Jenkins.getInstance()
def publish_ssh = inst.getDescriptor("jenkins.plugins.publish_over_ssh.BapSshPublisherPlugin")
def configuration = new BapSshHostConfiguration(name,
  hostname,
  username,
  encryptedPassword,
  remoteRootDir,
  port,
  timeout,
  overrideKey,
  keyPath,
  key,
  disableExec
)
publish_ssh.addHostConfiguration(configuration)
publish_ssh.save()