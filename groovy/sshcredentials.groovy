#!groovy

// imports
import java.util.logging.Level
import java.util.logging.Logger
import com.cloudbees.jenkins.plugins.sshcredentials.impl.*
import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.common.*
import com.cloudbees.plugins.credentials.domains.Domain
import com.cloudbees.plugins.credentials.impl.*
import hudson.util.Secret
import java.nio.file.Files
import jenkins.model.Jenkins
import net.sf.json.JSONObject
import org.jenkinsci.plugins.plaincredentials.impl.*

// get Jenkins instance
Jenkins jenkins = Jenkins.getInstance()
def logger = Logger.getLogger(Jenkins.class.getName())

// parameters
def sshkey = new File("/run/secrets/JenkinsSsh").text

def jenkinsMasterKeyParameters = [
  description:  'VagrantDockerManagerSSHKey',
  id:           'vagrant-docker-manager-key',
  userName:     'vagrant',
  key:          new BasicSSHUserPrivateKey.DirectEntryPrivateKeySource(sshkey)
]

logger.log(Level.INFO, "Ensuring SSH key is added, rsa key")
// get credentials domain
def domain = Domain.global()

// get credentials store
def store = jenkins.getExtensionList('com.cloudbees.plugins.credentials.SystemCredentialsProvider')[0].getStore()

// define private key
def privateKey = new BasicSSHUserPrivateKey(
  CredentialsScope.GLOBAL,
  jenkinsMasterKeyParameters.id,
  jenkinsMasterKeyParameters.userName,
  jenkinsMasterKeyParameters.key,
  null,
  jenkinsMasterKeyParameters.description
)

// add credential to store
store.addCredentials(domain, privateKey)

logger.log(Level.INFO, "SSH KEY added")

// save to disk
jenkins.save()