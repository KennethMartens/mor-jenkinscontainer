#!groovy

import hudson.model.Node.Mode
import hudson.slaves.*
import jenkins.model.Jenkins
import hudson.plugins.sshslaves.SSHLauncher
import hudson.plugins.sshslaves.*

//Handy debug logging
Jenkins.instance.nodes.each {
    println "BEFORE - Agent: $it"
}

// The "build" object is added by the Jenkins Groovy plugin and can resolve parameters and such
// ToDo: Make parameterized instead of hardcoded.
String credentialID = "vagrant-docker-manager-key"   // id of the key we created
String agentName = "swarm"
String agentDescription = "An Example Jenkins slave"
// Hardcoded DM1 Ip for now. TODO: CHANGE
String agentIP = "192.168.15.4"

// Other parameters are just hard coded for now, adjust as needed
String agentHome = "/var/jenkins"
String agentExecutors = 3
String agentLabels = "swarm"
if (agentName.contains("engine")) {
    agentLabels += " engine"
    println "That agent was for an engine builder, so updated label: $agentLabels"
}

// There is a constructor that also takes a list of properties (env vars) at the end, but haven't needed that yet
DumbSlave dumb = new DumbSlave(agentName, // Agent name, usually matches the host computer's machine name
        agentDescription, // Agent description
        agentHome, // Workspace on the agent's computer
        agentExecutors, // Number of executors
        Mode.EXCLUSIVE, // "Usage" field, EXCLUSIVE is "only tied to node", NORMAL is "any"
        agentLabels, // Labels
        new SSHLauncher(agentIP, 22, SSHLauncher.lookupSystemCredentials(credentialID), "", null, null, "", "", 60, 3, 15),
        RetentionStrategy.INSTANCE) // Is the "Availability" field and INSTANCE means "Always"
Jenkins.instance.addNode(dumb)
println "Agent '$agentName' created with $agentExecutors executors and home '$agentHome'"

Jenkins.instance.nodes.each {
    println "AFTER - Agent: $it"
}